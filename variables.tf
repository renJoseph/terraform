variable "image" {
  type        = map(any)
  description = "Container image"
  default = {
    nodered = {
      dev  = "nodered/node-red:latest"
      prod = "nodered/node-red:latest-minimal"
    },
    influxdb = {
      dev  = "quay.io/influxdb/influxdb:v2.0.2"
      prod = "quay.io/influxdb/influxdb:v2.0.2"
    },
    grafana = {
      dev  = "grafana/grafana:latest"
      prod = "grafana/grafana:latest"
    }
  }
}

variable "external_port" {
  type = map(any)
  # validation {
  #   condition     = max(var.external_port["dev"]...) <= 65535 && min(var.external_port["dev"]...) > 0
  #   error_message = "The external port is outside of valid range: [0 : 65535]."
  # }
  # validation {
  #   condition     = max(var.external_port["prod"]...) <= 65535 && min(var.external_port["prod"]...) > 0
  #   error_message = "The external port is outside of valid range: [0 : 65535]."
  # }
}

variable "internal_port" {
  type    = number
  default = 1880
  # validation {
  #   condition     = var.internal_port == 1880
  #   error_message = "The internal port must be 1880."
  # }
}
