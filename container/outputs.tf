# output "nodered-container-name" {
#   value       = docker_container.container.name
#   description = "The Nodered container name"
# }

# output "ip-address" {
#   value       = [for i in docker_container.container[*] : join(":", [i.ip_address], i.ports[*]["external"])]
#   description = "The nodered container IP address and external port"
# }

output "application_access" {
  value = {
    for x in docker_container.container[*] : x.name => join(":", [x.ip_address], x.ports[*]["external"])
  }
}
